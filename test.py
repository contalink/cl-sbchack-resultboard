import json
import os
from lambda_function import lambda_handler

def get_event_json():
    with open('request.json') as json_data:
        d = json.load(json_data)
    return d

def set_env_vars():
    os.environ['CL_API_URL'] = 'https://vru6erjswd.execute-api.us-east-1.amazonaws.com/contalink_prod/cl-util-call-function'

set_env_vars()
event = get_event_json()
response = lambda_handler(event, None)
print("Response Final")
print(json.dumps(response))
