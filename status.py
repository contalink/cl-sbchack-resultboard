import json
import requests
import os
import datetime
from util import build_request_body, build_request_headers, remove_non_numeric

def build_actual_status(params):
    actual_status = {}
    operational_info = get_company_info(params, 'company_monthly_operational_status')
    accounting_info = get_company_info(params, 'company_monthly_accounting_status')
    cashflow_info = get_company_info(params, 'company_monthly_cashflow_status')
    actual_status['operational_info'] = operational_info['operational_info']
    actual_status['accounting_info'] = accounting_info['accounting_info']
    actual_status['cashflow_info'] = cashflow_info['cashflow_info']
    actual_status['company_name'] = operational_info['company_name']
    actual_status['yearly_sales'] = calculate_yearly_sales(operational_info['operational_info'])

    actual_status['average_bank_balance'] = calculate_average_bank_balance(cashflow_info['cashflow_info'])
    yearly_profit, profit_margin = calculate_yearly_profit(accounting_info['accounting_info'])
    actual_status['yearly_profit'] = yearly_profit
    actual_status['profit_margin'] = profit_margin
    actual_status['accountant'] = operational_info['accountant']
    actual_status['company_age'] = calculate_company_age(params['rfc'])
    actual_status['credit_score'] = params['credit_score']
    actual_status['yearly_liability'] = calculate_yearly_liability(accounting_info['accounting_info'])
    actual_status['dscr'] = calculate_dscr(accounting_info['accounting_info'], yearly_profit)
    actual_status['debt_index'] = calculate_debt_index(accounting_info['accounting_info'])
    actual_status['debt_index_opinion'] = calculate_dscr_opinion(actual_status['dscr'])
    return actual_status

def get_company_info(params, request_type):
    url = os.environ['CL_API_URL']
    request_body = build_request_body(params, request_type)
    request_headers = build_request_headers(params)
    response = requests.request("POST", url, data=json.dumps(request_body), headers=request_headers)
    return json.loads(response.text)

def calculate_average_bank_balance(cashflow_info):
    months = 0
    total_balance = 0
    for month in cashflow_info:
        months = months + 1
        for balance in month['bank_balances']:
            total_balance = total_balance + balance['average_balance']
    return round(total_balance / months,2)

def calculate_yearly_profit(monthly_accounting_status):
    profit = 0
    yearly_income = 0
    yearly_expense = 0
    for month in monthly_accounting_status:
        profit = profit + month['income'] - month['expense']
        yearly_income = yearly_income + month['income']
        yearly_expense = yearly_expense + month['expense']
    return round(profit,2), round((1-(yearly_expense / yearly_income)) * 100,2)

def calculate_dscr_opinion(debt_index):
    if (debt_index <= 0.5):
        return "MUY MALO"
    if (debt_index > 0.5 and debt_index <= 1):
        return "MALO"
    if (debt_index > 1 and debt_index <= 1.5):
        return "REGULAR"
    if (debt_index > 1.5 and debt_index <= 2):
        return "BUENO"
    if (debt_index > 2):
        return "MUY BUENO"
    return None

def calculate_debt_index(monthly_accounting_status):
    for month in monthly_accounting_status:
        debt_index = month['liabilities'] / month['assets']
        return round(debt_index,2)

def calculate_company_age(rfc):
    non_numeric = remove_non_numeric(rfc)
    foundation_year = int(non_numeric[:2])
    if (foundation_year < 20):
        foundation_year = foundation_year + 2000
    else:
        foundation_year = foundation_year + 1900

    now = datetime.datetime.now()
    age = now.year - foundation_year
    return age

def calculate_yearly_sales(operational_info):
    sales = 0
    for month in operational_info:
        sales = sales + month['sales']
    return sales

def calculate_yearly_liability(accounting_info):
    for month in accounting_info:
        return month['liabilities']

def calculate_dscr(accounting_info, yearly_profit):
    total_liability = accounting_info[0]['liabilities']
    dscr = yearly_profit / total_liability
    return round(dscr, 2)
