import json
import boto3

def build_offers(params, actual_status):
    offers = []
    financial_institution_config = get_offer_rules()
    for financial_institution in financial_institution_config:
        offer = calculate_offer(financial_institution, params, actual_status)
        if (offer is not None):
            offers.append(offer)
    return offers

def get_offer_rules():
    dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
    table = dynamodb.Table('financial_institution_config')
    response = table.scan()
    return response['Items']

def calculate_offer(financial_institution, params, actual_status):
    offer = {}
    if (params['customer_type'] == 'company'):
        rule_type = 'company_rules'

    rule = financial_institution[rule_type]

    has_offer = can_receive_offer(rule, actual_status, actual_status['dscr'])
    return build_offer(has_offer, rule, financial_institution['financial_institution'], actual_status)

def can_receive_offer(rule, actual_status, dscr):
    if (dscr < 1):
        return False
    if (rule['minimum_cashflow_available'] > actual_status['average_bank_balance']):
        return False
    if (rule['minimum_credit_score'] > actual_status['credit_score']):
        return False
    if (rule['minimum_income'] > actual_status['yearly_sales']):
        return False
    return True

def build_offer(has_offer, rule, financial_institution, actual_status):
    offer = {}
    offer['financial_institution'] = financial_institution
    offer['expires'] = '01-01-2020'
    offer['code'] = 'n819h1398h'
    offer['rate'] = float(rule['rate'])
    offer['rate_term'] = rule['rate_term']
    offer['credit_term'] = float(rule['credit_term'])
    offer['credit_cat'] = float(rule['credit_cat'])
    offer['dscr'] = actual_status['dscr']
    if (has_offer is True):
        offer['amount'] = round((actual_status['yearly_profit'] - actual_status['yearly_liability']) * (float(rule['percentage_of_dscr']) / 100),2)
    else:
        offer['amount'] = 0

    return offer
