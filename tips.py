import json

def build_tips(params):
    tips = {}
    tips['cashflow'] = needs_cashflow_tip(params)
    tips['credit_score'] = needs_creditscore_tip(params)
    return tips

def needs_cashflow_tip(params):
    return True

def needs_creditscore_tip(params):
    return False
