import os
import json
import re

def create_message(status, messageText, logMessage):
    message = {}
    message['status'] = status
    message['message'] = messageText
    if(logMessage):
        print(json.dumps(message))
    return message

def build_request_body(params, request_type):
    request_body = {}
    request_body['function_name'] = request_type
    request_params = {}
    request_params['rfc'] = params['rfc']
    request_params['months_behind'] = params['months_behind']
    request_body['function_param'] = request_params
    return request_body

def build_request_headers(params):
    request_headers = {}
    request_headers['Authorization'] = params['authorization']
    request_headers['Content-Type'] = 'application/json'
    return request_headers

def remove_non_numeric(rfc):
    return re.sub('[^0-9]','', rfc)
