import boto3
import json
import traceback
from status import build_actual_status
from tips import build_tips
from offers import build_offers
from util import create_message

def lambda_handler(event, context):
    response = {}
    try:
        params = event['body-json']
        response = {}
        response['actual_status'] = build_actual_status(params)
        response['reccomendations'] = build_tips(params)
        response['offers'] = build_offers(params, response['actual_status'])
        return response

    except Exception as ex:
        traceback.print_exc()
        response = create_message(0, str(ex), True)
        print(json.dumps(response))
        return response
